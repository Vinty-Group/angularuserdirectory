import {Component, HostListener, inject, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {IPost} from "../../admin/interfaces";
import {PostService} from "../../shared/services/post.service";
import {BehaviorSubject, EMPTY, Observable, switchMap, tap} from "rxjs";
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(private titleService: Title) { }

    postService = inject(PostService);
    router = inject(Router);

    start$: BehaviorSubject<string> = new BehaviorSubject<string>('init');
    postData: Observable<IPost[]>;

    ngOnInit(): void {
        this.titleService.setTitle('Телефонный справочник');
        // при старте спрашиваем БД и получаем весь список
        // можно было вообще тупо спросить БД, но что бы корректно работало у админа и было все однообразно
        // через подпись. Избыточно даже.
        this.getPosts();

        // this.postService.saveBoolean("isLogin", true)
        // this.postService.saveLog()

    }

    getPosts() // хоум пейдж юзает метод и получается все данные
    {
        this.postData = this.start$.pipe(switchMap(() => this.postService.getOfficeAll()
                .pipe(tap(v => this.postService.currentData$.next(v)))));
    }

  // getPosts() // хоум пейдж юзает метод и получается все данные
  // {
  //   this.postData = this.postService.getAll().pipe(tap(v => this.postService.currentData$.next(v)));
  // }
}
