import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import {PostPageComponent} from "./components/post-page/post-page.component";
import { ModalPageComponent } from './components/modal-page/modal-page.component';
import { FilterHeadersPipe } from './pipes/filter-headers.pipe';
import { GroupPostsPipe } from './pipes/group-posts.pipe';
import {CdkDrag, CdkDropList} from "@angular/cdk/drag-drop";
import {AngularSvgIconModule} from "angular-svg-icon";
import { FilterPostsPipe } from './pipes/filter-posts.pipe';
import {ReactiveFormsModule} from "@angular/forms";
import { HighlightTextPipe } from './pipes/highlight-text.pipe';
import { TabsComponent } from './components/tabs/tabs.component';
import { PostTraderPageComponent } from './components/post-trader-page/post-trader-page.component';
import { PartnersPageComponent } from './components/partners-page/partners-page.component';
import { FilterPostsTradersPipe } from './pipes/filter-posts-traders.pipe';
import { FilterPostsPartnersPipe } from './pipes/filter-posts-partners.pipe';
import { SortColumnPipe } from './pipes/sort-column.pipe';
import { ModalBasicComponent } from './components/modal-basic/modal-basic.component';
import {PostPortativPageComponent} from "./components/post-portativ-page/post-portativ-page.component";
import { PostPageFiredComponent } from './components/post-page-fired/post-page-fired.component';

@NgModule({
  declarations: [
    PostPageComponent,
    ModalPageComponent,
    FilterHeadersPipe,
    GroupPostsPipe,
    FilterPostsPipe,
    HighlightTextPipe,
    TabsComponent,
    PostTraderPageComponent,
    PostPortativPageComponent,
    PartnersPageComponent,
    FilterPostsTradersPipe,
    FilterPostsPartnersPipe,
    SortColumnPipe,
    ModalBasicComponent,
    PostPageFiredComponent,
  ],
    exports: [
        PostPageComponent,
        ModalPageComponent,
        FilterHeadersPipe,
        TabsComponent,
        ModalBasicComponent
    ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    CdkDropList,
    CdkDrag,
    AngularSvgIconModule.forRoot(),
    ReactiveFormsModule,
    NgOptimizedImage
  ]
})
export class SharedModule { }
