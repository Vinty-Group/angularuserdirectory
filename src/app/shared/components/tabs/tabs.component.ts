import { Component, EventEmitter, HostListener, inject, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ITabs } from '../../interfaces';
import { IPost } from '../../../admin/interfaces';
import { Router } from '@angular/router';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent {
  router = inject(Router);
  postService = inject(PostService);
  isAdminMode = false;
  currentTab: number = 1;
  dashboardAdmin = false;

  @HostListener('document:keydown.Alt.Control.Shift.a', ['$event'])
  @HostListener('document:keydown.Alt.Control.Shift.ф', ['$event'])
  showAdmin(event) {
    this.isAdminMode = true;
  }

  @Input() posts: IPost[];
  @Input() isAdmin: boolean;
  @Input() start$: BehaviorSubject<string>;

  @Output() remove: EventEmitter<IPost> = new EventEmitter<IPost>();
  @Output() post: EventEmitter<IPost> = new EventEmitter<IPost>();
  @Output() tabChange: EventEmitter<number> = new EventEmitter<number>(); // Добавлено это

  tabs: ITabs[] = [
    { id: 1, name: 'Офис, склад' },
    { id: 2, name: 'ТО На Связи' },
    { id: 4, name: 'ТО Портатив' },
    { id: 3, name: 'Партнеры' },
    { id: 6, name: 'Уволенные' },
  ];

  ngOnInit(): void {
    this.checkAdminAccess();
  }

  checkAdminAccess(): void {
    let isLogin = this.postService.checkLogin();

    if (isLogin) {
      if (!this.tabs.find(tab => tab.id === 5)) {
        this.tabs.push({ id: 5, name: 'Админ' });
      }
      if (!this.tabs.find(tab => tab.id === 6)) {
        this.tabs.push({ id: 6, name: 'Уволенные' });
      }
    } else {
      this.tabs = this.tabs.filter(tab => tab.id !== 6);
    }
  }

  onTabClick(tab: ITabs): void {
    if (tab.id === 5) {
      this.toggleAdminRoute();
    } else {
      this.currentTab = tab.id;
      this.tabChange.emit(this.currentTab); // Излучаем текущий ID вкладки
    }
  }

  asLogin() {
    this.dashboardAdmin = this.postService.isDashboardActive();
    const tab5 = this.tabs.find(tab => tab.id === 5);
  }

  toggleAdminRoute(): void {
    this.asLogin()

    if (this.dashboardAdmin) {
      this.router.navigate(['/']).then(() => { });
    } else {
      this.router.navigate(['/admin/dashboard']).then(() => { });
    }
  }
}
