import {Component, inject} from '@angular/core';
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-modal-page',
  templateUrl: './modal-page.component.html',
  styleUrls: ['./modal-page.component.css']
})
export class ModalPageComponent {
  modalService = inject(ModalService);
  closeModal() {
    this.modalService.closeModalAll();
  }
}
