import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {IPost} from "../../../admin/interfaces";
import {PostService} from "../../services/post.service";
import {BehaviorSubject} from "rxjs";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DEBUG} from "@angular/compiler-cli/src/ngtsc/logging/src/console_logger";

@Component({
  selector: 'app-post-page-fired',
  templateUrl: './post-page-fired.component.html',
  styleUrls: ['./post-page-fired.component.css']
})
export class PostPageFiredComponent {
  @ViewChild('filterInput') filterInput: ElementRef;
  @Input() posts: IPost[];
  @Input() isAdmin = false;
  @Input() start$: BehaviorSubject<string>;

  @Output() remove: EventEmitter<IPost> = new EventEmitter<IPost>();
  @Output() post: EventEmitter<IPost> = new EventEmitter<IPost>();

  filterForm: FormGroup;
  postService = inject(PostService);
  fb = inject(FormBuilder);

  ngOnInit(): void
  {
    this.filterForm = this.fb.group({
      filter: []
    });
  }

  ngAfterViewInit(): void {
    this.filterInput.nativeElement.focus();
  }
  // removePost(event, post: IPost)
  // {
  //     event.stopPropagation();
  //     this.remove.emit(post);
  // }

  rowClick(post: IPost)
  {
    this.post.emit(post);
  }

  // если у заголовка есть дети - true
  haveChildren(post)
  {
    let iPosts = this.postService.currentData$.getValue().filter(f => f.parent === post.id);
    return iPosts.length > 0;
  }

  transform(posts: IPost[]): IPost[]
  {
    const currentPosts = this.postService.currentData$.getValue();
    if (!currentPosts) {
      return [];
    }
    return currentPosts
      .filter(p => p.typeRecord === 'header')
      .sort((a, b) => a.officeCells.jobTitle.localeCompare(b.officeCells.jobTitle))
      .map(post => [
        post,
        ...posts
          .filter(f => f.parent === post.id)
          .sort((a, b) => +a.order - +b.order)
      ])
      .flat();
  }
}
