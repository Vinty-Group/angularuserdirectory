import {Component, HostListener} from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent {
  isAdminMode = false;
  @HostListener('document:keydown.Alt.Control.Shift.a', ['$event'])
  showAdmin(event){
    this.isAdminMode = true;
  }
}
