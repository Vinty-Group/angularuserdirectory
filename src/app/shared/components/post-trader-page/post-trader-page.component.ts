import {AfterViewInit, Component, ElementRef, inject, Input, OnInit, ViewChild} from '@angular/core';
import {PostService} from "../../services/post.service";
import {Router} from "@angular/router";
import {BehaviorSubject, Observable, switchMap, tap} from "rxjs";
import {IPost, IPostTrader, ITrader} from "../../../admin/interfaces";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ModalBasicService} from "../../services/modal-basic.service";
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-post-trader-page',
  templateUrl: './post-trader-page.component.html',
  styleUrls: ['./post-trader-page.component.css']
})
export class PostTraderPageComponent implements OnInit, AfterViewInit {
  @ViewChild('filterInput') filterInput: ElementRef;
  @ViewChild('modal') modal: ElementRef;

  postDataTraders: Observable<IPostTrader[]>;
  start$: BehaviorSubject<string> = new BehaviorSubject<string>('init');

  selectedPost: IPostTrader;

  @Input() isAdmin = false;

  filterForm: FormGroup;
  modalForm: FormGroup;

  postService = inject(PostService);
  modalService = inject(ModalService);
  router = inject(Router);
  fb = inject(FormBuilder);

  public dashboardAdmin: boolean = false;

  sortColumn: string = '';

  ngOnInit(): void {
    this.initModalForm();
    this.getPostsTraders();
    this.sortTraders('codeTO');
    this.InitGetTraders();
    this.asLogin()
  }

  ngAfterViewInit(): void {
    this.filterInput.nativeElement.focus();
  }

  getPostsTraders() {
    this.postDataTraders = this.postService.getAllTraders()
      .pipe(tap(v => this.postService.currentDataTraders$.next(v)));
  }

  asLogin() {
    let isLogin = this.postService.checkLogin()
    let isDashboardActive = this.postService.isDashboardActive()
    this.dashboardAdmin = isDashboardActive && isLogin;
  }

  InitGetTraders() {
    return this.postDataTraders = this.start$.pipe(switchMap(() => this.postService.getAllTraders()));
  }

  sort(column: string, event: Event) {
    this.sortColumn = column;
  }

  sortTraders(column: string) {
    this.sortColumn = column;
  }

  initModalForm() // модальное окно
  {
    this.filterForm = this.fb.group({
      filter: []
    });

    this.modalForm = this.fb.group({
      codeTO: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required],
      place: ['', Validators.required],
      operator: ['', Validators.required],
      schedule: ['', Validators.required],
      phone: ['', Validators.required],
      headOfSector: ['', Validators.required]
    });
  }


  rowClick(post: IPostTrader) {
    if (!this.dashboardAdmin) {
      return;
    }

    this.initModalForm()
    this.selectedPost = post;

    // Заполняем форму данными выбранного поста
    this.modalForm.patchValue({
      codeTO: post.traders.codeTO,
      city: post.traders.city,
      address: post.traders.address,
      place: post.traders.place,
      operator: post.traders.operator,
      schedule: post.traders.schedule,
      phone: post.traders.phone,
      headOfSector: post.traders.headOfSector
    });

    // Открываем модальное окно
    this.modalService.openTradeModal();
  }

  updateModalFormTrader() {
    const val = this.modalForm.value;  // Получаем данные формы
    const trader: ITrader = {
      codeTO: val?.codeTO,
      city: val?.city,
      address: val?.address,
      place: val?.place,
      operator: val?.operator,
      schedule: val?.schedule,
      phone: val?.phone,
      headOfSector: val?.headOfSector
    }

    const postTrader: IPostTrader = {
      id: this.selectedPost.id,
      traders: trader,
      date: new Date(),
      order: this.selectedPost.order || ''  // Проверка на пустое значение
    }

    // Возвращаем и обновляем данные через метод updateModalPostTrader
    return this.updateModalPostTrader(postTrader);
  }

  copyModalFormTrader() {
    const val = this.modalForm.value;  // Получаем данные формы
    const trader: ITrader = {
      codeTO: val?.codeTO,
      city: val?.city,
      address: val?.address,
      place: val?.place,
      operator: val?.operator,
      schedule: val?.schedule,
      phone: val?.phone,
      headOfSector: val?.headOfSector
    }

    const postTrader: IPostTrader = {
      id: this.selectedPost.id,
      traders: trader,
      date: new Date(),
      order: this.selectedPost.order || ''  // Проверка на пустое значение
    }
    return this.copyModalPostTrader(postTrader);
  }

  deleteRecordModal() // модальное окно
  {
    const val = this.modalForm.value;
    this.postService.removeTrader(this.selectedPost.id).subscribe(value => {
      this.start$.next('deleted');
    })
    this.modalService.closeTradeModal();
    // this.removeModal(val.id);
  }

  updateModalPostTrader(postTrader: IPostTrader) {
    this.postService.updatePostTrader(postTrader) // апдейтим пост
      .subscribe(() => {
          this.modalService.closeTradeModal();
          this.start$.next('saved');
        }
      )
  };

  copyModalPostTrader(postTrader: IPostTrader) {
    this.postService.savePostTrader(postTrader) // апдейтим пост
      .subscribe(() => {
          this.modalService.closeTradeModal();
          this.start$.next('saved');
        }
      )
  };

}
