import {Component, inject, Input, OnInit} from '@angular/core';
import {ModalBasicService} from "../../services/modal-basic.service";

@Component({
  selector: 'app-modal-basic',
  templateUrl: './modal-basic.component.html',
  styleUrls: ['./modal-basic.component.css']
})
export class ModalBasicComponent{
  @Input() title: string = 'Title';
  @Input() message: string = 'Message';
  modalBasicService = inject(ModalBasicService);

  closeModal() {
    this.modalBasicService.isOpenModal1.next(false);
  }
}
