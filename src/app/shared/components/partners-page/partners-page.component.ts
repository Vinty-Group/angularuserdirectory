import {AfterViewInit, Component, ElementRef, inject, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable, switchMap, tap} from "rxjs";
import {IPartners, IPostPartners, IPostTrader, ITrader} from "../../../admin/interfaces";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PostService} from "../../services/post.service";
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-partners-page',
  templateUrl: './partners-page.component.html',
  styleUrls: ['./partners-page.component.css']
})
export class PartnersPageComponent implements OnInit, AfterViewInit {
  @ViewChild('filterInput') filterInput: ElementRef;

  postDataPartners: Observable<IPostPartners[]>;
  start$: BehaviorSubject<string> = new BehaviorSubject<string>('init');

  filterForm: FormGroup;
  modalForm: FormGroup;
  selectedPost: IPostPartners;
  public dashboardAdmin: boolean = false;

  sortColumn: string = '';

  postService = inject(PostService);
  modalService = inject(ModalService);
  fb = inject(FormBuilder);

  ngOnInit(): void {
    this.initForms();
    this.getPostsPartners();
    this.InitGetStartPartners();
    this.asLogin();
  }

  ngAfterViewInit(): void {
    this.filterInput.nativeElement.focus();
  }

  sortPostsByOrder(posts: IPostPartners[]): IPostPartners[] {
    return posts.sort((a, b) => (a.order || 0) - (b.order || 0));
  }

  initForms() {
    this.filterForm = this.fb.group({
      filter: []
    });

    this.modalForm = this.fb.group({
      serviceName: ['', Validators.required],
      bankName: ['', Validators.required],
      fullName: ['', Validators.required],
      phone: ['', Validators.required],
      description: ['', Validators.required],
      order: ['', Validators.required]
    });
  }

  // InitGetStartPartners() {
  //   return this.postDataPartners = this.start$.pipe(switchMap(() => this.postService.getAllPartners()));
  // }

  InitGetStartPartners() {
    this.postDataPartners = this.start$.pipe(
      switchMap(() => this.postService.getAllPartners()),
      tap(posts => this.sortPostsByOrder(posts))
    );
  }
  asLogin() {
    let isLogin = this.postService.checkLogin()
    let isDashboardActive = this.postService.isDashboardActive()
    this.dashboardAdmin = isDashboardActive && isLogin;
  }

  // getPostsPartners2() {
  //   this.postDataPartners = this.postService.getAllPartners()
  //     .pipe(tap(v => this.postService.currentDataPartners$.next(v)));
  // }

  getPostsPartners() {
    this.postDataPartners = this.postService.getAllPartners()
      .pipe(
        tap(v => {
          const sortedData = this.sortPostsByOrder(v);
          this.postService.currentDataPartners$.next(sortedData);
        })
      );
  }

  rowClick(post: IPostPartners) {
    if (!this.dashboardAdmin) {
      return;
    }

    this.selectedPost = post;

    // Заполняем форму данными выбранного поста
    this.modalForm.patchValue({
      serviceName: post.partners.serviceName,
      bankName: post.partners.bankName,
      fullName: post.partners.fullName,
      phone: post.partners.phone,
      description: post.partners.description,
      order: post.order
    });

    // Открываем модальное окно
    this.modalService.openPartnerModal()
  }

  updateModalFormPartners() {
    const val = this.modalForm.value;  // Получаем данные формы
    const partner: IPartners = {
      serviceName: val?.serviceName,
      bankName: val?.bankName,
      fullName: val?.fullName,
      phone: val?.phone,
      description: val?.description
    }

    const postPartners: IPostPartners = {
      id: this.selectedPost.id,
      partners: partner,
      date: new Date(),
      order: val.order ? Number(val.order) : '' // Явное приведение к числу
    }

    // Возвращаем и обновляем данные через метод updateModalPostTrader
    return this.updateModalPostPartners(postPartners);
  }

  copyModalFormPartners() {
    const val = this.modalForm.value;  // Получаем данные формы
    const partner: IPartners = {
      serviceName: val?.serviceName,
      bankName: val?.bankName,
      fullName: val?.fullName,
      phone: val?.phone,
      description: val?.description
    }

    const postPartners: IPostPartners = {
      id: this.selectedPost.id,
      partners: partner,
      date: new Date(),
      order: val.order ? Number(val.order) : '' // Явное приведение к числу
    }

    // Возвращаем и обновляем данные через метод updateModalPostTrader
    return this.copyModalPostPartners(postPartners);
  }

  updateModalPostPartners(postPartners: IPostPartners) {
    this.postService.updatePostPartners(postPartners) // апдейтим пост
      .subscribe(() => {
          this.modalService.closePartnerModal();
          this.start$.next('saved');
        }
      )
  };

  copyModalPostPartners(postPartners: IPostPartners) {
    this.postService.savePostPartners(postPartners) // апдейтим пост
      .subscribe(() => {
          this.modalService.closePartnerModal();
          this.start$.next('saved');
        }
      )
  };


  updateModalForm() {
    // Обновите запись в вашем сервисе или где необходимо
    this.modalService.closePartnerModal();
  }

  copyModalForm() {
    // Логика для копирования записи
    this.modalService.closePartnerModal();
  }

  deleteRecordPartner() // модальное окно
  {
    this.postService.removePartners(this.selectedPost.id).subscribe(value => {
      this.start$.next('deleted');
    })
    this.modalService.closePartnerModal();
  }
}
