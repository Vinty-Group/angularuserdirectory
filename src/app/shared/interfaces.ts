export interface IEnvironment {
    production: boolean
    apiKey: string
    fbDbUrl: string
}

export interface ITabs {
    id: number
    name: string
}


