import { Pipe, PipeTransform } from '@angular/core';
import {IPostTrader} from "../../admin/interfaces";

@Pipe({
  name: 'sortColumn'
})
export class SortColumnPipe implements PipeTransform {

  transform(value: IPostTrader[], sort: string): IPostTrader[] {
    if (!sort){
      return value;
    }
    return value
      .map(a => a)
      .sort((prev, next) => prev.traders[sort].localeCompare(next.traders[sort]));
  }
}
