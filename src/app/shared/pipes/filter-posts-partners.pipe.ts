import { Pipe, PipeTransform } from '@angular/core';
import {IPostPartners} from "../../admin/interfaces";


@Pipe({
  name: 'filterPostsPartners'
})
export class FilterPostsPartnersPipe implements PipeTransform {

  transform(value: IPostPartners[], filter: string): IPostPartners[] {
    if (!value?.length) {
      return [];
    }
    if (!filter) {
      return value || [];
    }
    const upperCaseFilter = filter.toUpperCase();
    return value.filter(f =>
      f.partners?.serviceName?.toUpperCase().includes(upperCaseFilter) ||
      f.partners?.bankName?.toUpperCase().includes(upperCaseFilter) ||
      f.partners?.fullName?.toUpperCase().includes(upperCaseFilter) ||
      f.partners?.phone?.includes(filter)
    );
  }
}
