import { Pipe, PipeTransform } from '@angular/core';
import {IPostTrader} from "../../admin/interfaces";

@Pipe({
  name: 'filterPostsTraders'
})
export class FilterPostsTradersPipe implements PipeTransform {

  transform(value: IPostTrader[], filter: string): IPostTrader[] {
    if (!value?.length){
      return  [];
    }
    if (!filter){
      return  value || [];
    }
    return value.filter(f =>
    f.traders?.city?.toUpperCase().includes(filter.toUpperCase())
        || f.traders?.phone?.includes(filter)
      || f.traders?.headOfSector?.toUpperCase().includes(filter.toUpperCase())
        || f.traders?.codeTO?.toUpperCase().includes(filter.toUpperCase())
        || f.traders?.address?.toUpperCase().includes(filter.toUpperCase())
        || f.traders?.place?.toUpperCase().includes(filter.toUpperCase())
        || f.traders?.operator?.toUpperCase().includes(filter.toUpperCase())
    )
  }
}
