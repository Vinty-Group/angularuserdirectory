import { Pipe, PipeTransform } from '@angular/core';
import {IPost} from "../../admin/interfaces";

@Pipe({
  name: 'filterHeaders'
})
export class FilterHeadersPipe implements PipeTransform {

  transform(value: IPost[]): IPost[] {
    return value.filter(post => post.typeRecord === 'header' || post.typeRecord === 'subHeader');
  }
}
