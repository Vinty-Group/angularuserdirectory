import { Pipe, PipeTransform } from '@angular/core';
import {IPost} from "../../admin/interfaces";

@Pipe({
  name: 'filterPosts'
})
export class FilterPostsPipe implements PipeTransform {

  // value - весь список
  // filter - значение из INPUT
  transform(value: IPost[], filter: string): IPost[] {
    if (!value?.length) {
      return [];
    }
    if (!filter) {
      return value || [];
    }
    return value.filter(f =>
        f.officeCells?.fullName?.toUpperCase().includes(filter.toUpperCase())
        || f.officeCells?.phone?.includes(filter)
        || f.officeCells?.jobTitle?.toUpperCase().includes(filter.toUpperCase()));
  }
}
