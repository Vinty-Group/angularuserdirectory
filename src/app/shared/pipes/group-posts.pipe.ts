import { Pipe, PipeTransform } from '@angular/core';
import {IPost} from "../../admin/interfaces";

@Pipe({
  name: 'groupPosts'
})
export class GroupPostsPipe implements PipeTransform {

  transform(posts: IPost[], ...args: unknown[]): IPost[] {
    if (!posts){
      return [];
    }
      // 1. filter создал новый массив из заголовков.
      // 2. sort их отсортировал по имени
      // 3. map - взял отсортированный массив из этих header и для каждого header сформировал массив из его самого и его детей
      // ...value - что бы не вставлять массив, а только его элементы
      // и мы фильтруем эти элементы выбирая лишь те, у кого id-header является папой
      // дальше сортируем по number в порядке возрастания
      // flat - содержимое всех массивов - сливает в один большой массив, что бы не было вложенности
    return posts
        .filter(p => p.typeRecord === 'header')
        .sort((a, b) => a
            .officeCells.jobTitle.localeCompare(b.officeCells.jobTitle, 'ru', {numeric: true}))
        .map(post => [
            post,
          ...posts
              .filter(f => f.parent === post.id)
              .sort((a, b) => +a.order - +b.order)
        ])
        .flat();
  }
}
