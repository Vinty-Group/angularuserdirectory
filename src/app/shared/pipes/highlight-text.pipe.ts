import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlightText'
})
export class HighlightTextPipe implements PipeTransform {

  transform(value: string, filter: string): string {
    if (!filter) {
      return value;
    }
    if (!value) {
      return '';
    }
    // igm - в регэкспе
    // i = игнорит кемел кейсы,
    // g = глобал, то есть ищет не до первого нашедшего а продолжает искать дальше,
    // m = мультилайн. Игнорит знаки корретки
    const reg = new RegExp(filter, 'igm');
    // $& = резкльтат рег-экспа. Надо только так.
    // то что нашло заменить на <span>.
    // изза этого в HTML надо использовать жесткую вкладку в DOM
    // [innerHTML]="post.officeCells.fullName | highlightText:filterForm.get('filter').value"
    return value.replace(reg, '<span class="highlighted">$&</span>');
  }
}
