import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, of, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModalBasicService {

  // Управление состоянием модальных окон
  isOpenModal1 = new BehaviorSubject<boolean>(false);
  isOpenModal2 = new BehaviorSubject<boolean>(false);

  // Для реакции на события модального окна
  modalReaction = new Subject<boolean>();

  // Объединяем состояния модальных окон
  isOpened = combineLatest([
    this.isOpenModal1,
    this.isOpenModal2,
  ])
    .pipe(
      switchMap(arr => of(arr.some(f => f)))
    );

  // Открытие модального окна 1
  openModal1(): void {
    this.isOpenModal1.next(true);
  }

  // Закрытие модального окна 1
  closeModal1(): void {
    this.isOpenModal1.next(false);
  }

  // Открытие модального окна 2
  openModal2(): void {
    this.isOpenModal2.next(true);
  }

  // Закрытие модального окна 2
  closeModal2(): void {
    this.isOpenModal2.next(false);
  }

  // Реакция на действие в модальном окне
  confirmModalAction(result: boolean): void {
    this.modalReaction.next(result);
  }
}
