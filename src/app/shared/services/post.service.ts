import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, map, Observable, switchMap, tap} from "rxjs";

import {IPost, IPostPartners, IPostTrader} from "../../admin/interfaces";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  http = inject(HttpClient);
  currentData$: BehaviorSubject<IPost[]> = new BehaviorSubject<IPost[]>([]);
  currentDataTraders$: BehaviorSubject<IPostTrader[]> = new BehaviorSubject<IPostTrader[]>([]);
  currentDataPartners$: BehaviorSubject<IPostPartners[]> = new BehaviorSubject<IPostPartners[]>([]);

  private dashboardActive: boolean = false;
  private readonly BASE_URL = 'http://192.168.5.26:9999';
  // private readonly BASE_URL = 'http://localhost:9999';

  updatePost(post: IPost): Observable<IPost> // тут апдейтим пост, из того что попало внутрь
  {
    return this.http.put<IPost>(`${this.BASE_URL}/updateOfficeCell`, post);
  }

  updatePostTrader(postTrader: IPostTrader): Observable<IPostTrader> // тут апдейтим пост, из того что попало
  {
    return this.http.put<IPost>(`${this.BASE_URL}/updateTraderCell`, postTrader);
  }

  updatePostPortativ(postTrader: IPostTrader): Observable<IPostTrader> // тут апдейтим пост, из того что попало
  {
    return this.http.put<IPost>(`${this.BASE_URL}/updatePortativCell`, postTrader);
  }

  updatePostPartners(postPartners: IPostPartners): Observable<IPostPartners> // тут апдейтим пост, из того что попало
  {
    return this.http.put<IPost>(`${this.BASE_URL}/updatePartnerCell`, postPartners);
  }

  savePost(post: IPost): Observable<IPost> {
    return this.http.post<IPost>(`${this.BASE_URL}/saveOfficeCell`, post);
  }

  savePostTrader(postTrader: IPostTrader): Observable<IPostTrader>
  {
    return this.http.post<IPost>(`${this.BASE_URL}/saveTraderCell`, postTrader);
  }

  savePostPortativ(postTrader: IPostTrader): Observable<IPostTrader>
  {
    return this.http.post<IPost>(`${this.BASE_URL}/savePortativCell`, postTrader);
  }

  savePostPartners(postPartners: IPostPartners): Observable<IPostPartners>
  {
    return this.http.post<IPost>(`${this.BASE_URL}/savePartnerCell`, postPartners);
  }

  remove(id: string) {
    return this.http.get(`${this.BASE_URL}/del/${id}`);
  }

  removeTrader(id: string) {
    return this.http.get(`${this.BASE_URL}/delTrader/${id}`);
  }

  removePortativ(id: string) {
    return this.http.get(`${this.BASE_URL}/delPortativ/${id}`);
  }

  removePartners(id: string) {
    return this.http.get(`${this.BASE_URL}/delPartner/${id}`);
  }

  getPosts() {
    this.getOfficeAll().pipe(tap(v => this.currentData$.next(v)));
  }

  getOfficeAll(): Observable<IPost[]> {
    return this.http.get(`${this.BASE_URL}/getOfficeCell`)
      .pipe(map((response: { [key: string]: any }) =>
        Object
          .keys(response)
          .map(key => ({
            ...response[key],
            id: response[key].id,
            date: new Date(response[key].date)
          }))
      ));
  }

  getAllTraders(): Observable<IPostTrader[]> {
    return this.http.get(`${this.BASE_URL}/getTraderCell`)
      .pipe(map((response: { [key: string]: any }) =>
        Object
          .keys(response)
          .map(key => ({
            ...response[key],
            id: response[key].id,
            date: new Date(response[key].date)
          }))
      ));
  }

  getAllPortativ(): Observable<IPostTrader[]> {
    return this.http.get(`${this.BASE_URL}/getPortativCell`)
      .pipe(map((response: { [key: string]: any }) =>
        Object
          .keys(response)
          .map(key => ({
            ...response[key],
            id: response[key].id,
            date: new Date(response[key].date)
          }))
      ));
  }

  getAllPartners(): Observable<IPostPartners[]> {
    return this.http.get(`${this.BASE_URL}/getPartnersCell`)
      .pipe(map((response: { [key: string]: any }) =>
        Object
          .keys(response)
          .map(key => ({
            ...response[key],
            id: key,
            date: new Date(response[key].date)
          }))
      ));
  }

  // Метод для сохранения значения в localStorage
  saveBoolean(key: string, value: boolean): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  // Метод для установки состояния дашборда
  setDashboardActive(isActive: boolean): void {
    this.dashboardActive = isActive;
  }

  // Метод для получения состояния дашборда
  isDashboardActive(): boolean {
    return this.dashboardActive;
  }

  getBoolean(key: string): boolean | null {
    const value = localStorage.getItem(key);
    if (value === null) {
      return null;
    }
    return JSON.parse(value);
  }

  checkLogin() {

    let result = this.getBoolean("isLogin")

    if (result === null) {
      return false;
    }
    return this.getBoolean("isLogin")
  }
}
