import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private isOpenSubject = new Subject<boolean>();
  private isOpenSubjectTrader = new Subject<boolean>();
  private isOpenSubjectPartners = new Subject<boolean>();
  isOpen$ = this.isOpenSubject.asObservable();
  isOpenTrader$ = this.isOpenSubjectTrader.asObservable();
  isOpenPartners$ = this.isOpenSubjectPartners.asObservable();

  openModal() {
    this.isOpenSubject.next(true);
  }

  closeModal() {
    this.isOpenSubject.next(false);
  }

  closeModalAll() {
    this.isOpenSubject.next(false);
    this.isOpenSubjectTrader.next(false);
    this.isOpenSubjectPartners.next(false);
  }
  openPartnerModal() {
    this.isOpenSubjectPartners.next(true);
  }

  closePartnerModal() {
    this.isOpenSubjectPartners.next(false);
  }

  openTradeModal() {
    this.isOpenSubjectTrader.next(true);
  }

  closeTradeModal() {
    this.isOpenSubjectTrader.next(false);
  }
}
