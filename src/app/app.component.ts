import {Component, inject, OnInit} from '@angular/core';
import {ModalBasicService} from "./shared/services/modal-basic.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  modalBasicService = inject(ModalBasicService);
  ngOnInit(): void {
    this.modalBasicService.isOpened.subscribe(res => {});
    // setTimeout(() => this.modalBasicService.isOpenModal1.next(true), 1000);
  }
}
