export interface IUser {
    email: string
    password: string
    returnSecureToken?: boolean
}

export interface IFbAuthResponse {
    idToken: string,
    expiresIn: string
}

export interface IPost {
    id?: string,
    typeRecord?: string,
    officeCells: IOfficeCells,
    date?: Date,
    parent?: string,
    order?: number|'',
    firedStatus?: number|''

}
export interface IOfficeCells {
    jobTitle?: string,
    responsibleFor?: string,
    fullName?: string,
    phone?: string
}
export interface IPostTrader {
    id?: string,
    traders?: ITrader,
    date?: Date,
    order?: number | ''
}
export interface ITrader {
    codeTO?: string,
    city?: string,
    address?: string,
    place?: string,
    operator?: string,
    schedule?: string,
    phone?: string,
    headOfSector?: string
}

export interface IPostPartners {
  id?: string,
  partners?: IPartners,
  date?: Date,
  order?: number | ''
}
export interface IPartners {
  serviceName?: string,
  bankName?: string,
  fullName?: string,
  phone?: string,
  description?: string,
}
export interface FbCreateResponse {
    name: string
}

export interface IHeader {
    id: string,
    name: string
}
