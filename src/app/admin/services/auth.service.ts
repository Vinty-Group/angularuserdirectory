import {inject, Injectable} from '@angular/core';
import {IFbAuthResponse, IUser} from "../interfaces";
import {catchError, EMPTY, Observable, Subject, tap} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({ providedIn: 'root' })
export class AuthService {
  public error$: Subject<string> = new Subject<string>();

  http = inject(HttpClient);

  login(user: IUser): Observable<any> {
    user.returnSecureToken = true;
    return this.http
        .post(
            `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`,
            user
        )
        .pipe(tap(this.setToken), catchError(this.handleError.bind(this)));
  }
  logout() {
    this.setToken(null);
  }

  get token(): string {
    const expDate: Date = new Date(localStorage.getItem('fb-token-exp'));
    if (new Date() > expDate) {
      this.logout();
      return null;
    }
    return localStorage.getItem('fb-token');
  }
  private setToken(response: IFbAuthResponse | null) {
    if (response) {
      const expDate = new Date(
          new Date().getTime() + +response.expiresIn * 28800
      );
      localStorage.setItem('fb-token', response.idToken);
      localStorage.setItem('fb-token-exp', expDate.toString());
    } else {
      localStorage.setItem('fb-token', '');
      localStorage.setItem('fb-token-exp', '');
    }
  }
  // isAuthenticated(): boolean {
  //   return !!this.token;
  // }

  isAuthenticated(): boolean {
    return true;
  }

  private handleError(error: HttpErrorResponse) {
    const { message } = error.error.error;
    switch (message) {
      case 'INVALID_EMAIL':
        this.error$.next('Неверный EMAIL')
        break;
      case 'INVALID_PASSWORD':
        this.error$.next('Неверные имя пользователя или пароль')
        break;
      case 'EMAIL_NOT_FOUND':
        this.error$.next('Такой EMAIL не существует')
        break;
    }
    return EMPTY;
  }
}
