import {inject, Injectable} from '@angular/core';
import {IHeader, IPost} from "../interfaces";
import {PostService} from "../../shared/services/post.service";
import {BehaviorSubject, Observable, switchMap, tap} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ExchangeService {

    headers: IHeader[] = [];
    postService = inject(PostService);

    // maxOrder: IPost;

    // fillParents(): IHeader[]
    // {
    //     this.headers = [];
    //     this.postService.currentData$.subscribe(res =>
    //     {
    //         this.headers = res
    //             .filter(post => post.typeRecord === 'header') // сравниваем что если пост
    //             .map(post => ({
    //                 id: post.id,
    //                 name: post.officeCells.jobTitle
    //             }));
    //     });
    //     return this.headers;
    // }


  fillParents(): IHeader[] {
    this.headers = [];
    this.postService.currentData$.subscribe(res => {
      this.headers = res
        .filter(post => post.typeRecord === 'header')
        .map(post => ({
          id: post.id,
          name: post.officeCells.jobTitle
        }));

      // Консольный вывод внутри подписки
    });
    // Консольный вывод сразу после вызова subscribe
    return this.headers;
  }
}
