import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {AdminLayoutComponent} from "./component/admin-layout/admin-layout.component";
import {LoginPageComponent} from "./component/login-page/login-page.component";
import { DashboardPageComponent } from './component/dashboard-page/dashboard-page.component';
import { CreatePageComponent } from './component/create-page/create-page.component';
import { EditPageComponent } from './component/edit-page/edit-page.component';
import {NgSelectModule} from "@ng-select/ng-select";
import {SharedModule} from "../shared/shared.module";

@NgModule({
    declarations: [
    DashboardPageComponent,
    CreatePageComponent,
    EditPageComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: "", component: AdminLayoutComponent,
                children: [
                    {path: "", redirectTo: "/admin/login", pathMatch: "full"},
                    {path: "login", component: LoginPageComponent},
                    {path: "dashboard", component: DashboardPageComponent},
                    {path: "create", component: CreatePageComponent},
                    {path: "post/:id/edit", component: EditPageComponent},
                ]
            }
        ]),
        NgSelectModule,
        SharedModule,
    ],
    exports: [],
})
export class AdminModule {
}
