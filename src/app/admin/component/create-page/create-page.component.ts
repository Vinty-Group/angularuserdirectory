import { Component, inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IHeader, IOfficeCells, IPost } from '../../interfaces';
import { PostService } from '../../../shared/services/post.service';
import { ExchangeService } from '../../services/exchange.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NgSelectModule } from '@ng-select/ng-select';
import {Location} from "@angular/common";

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.css']
})
export class CreatePageComponent implements OnInit {
  constructor(private location: Location) {}
  form: FormGroup;
  fb = inject(FormBuilder);
  postService = inject(PostService);
  exchangeService = inject(ExchangeService);
  router = inject(Router);

  postData: Observable<IPost[]>; // все данные сразу пишутся сюда, в том числе айди
  typeRecord = [
    { id: 'normal', name: 'Нормальная' },
    { id: 'header', name: 'Заголовок' }
  ];
  headers: IHeader[] = [];
  filteredHeaders: IPost[] = [];

  ngOnInit(): void {
    this.postData = this.postService.getOfficeAll();
    this.fillParentsFromService();
    this.initForm();
    this.filterHeaders();
    this.getCurrentRouteInfo();

    this.asLogin()
  }

  asLogin () {
    let isLogin = this.postService.checkLogin()

    if (!isLogin) {
      // Перенаправить на другой маршрут
      this.router.navigate(['/admin/login']).then(r => { this.logHelloWorld()  });
    }
  }

  getCurrentRouteInfo(): void {
    // Получить информацию о маршруте из Router

    // Получить информацию о параметрах маршрута
  }

  initForm() {
    this.form = this.fb.group({
      typeRecord: [this.typeRecord[0].id],
      jobTitle: [null, Validators.required],
      responsibleFor: [null],
      fullName: [null],
      phone: [null],
      OrderRecord: [null],
      parent: [null]
    });
  }

  createPost() {
    if (this.form.invalid) {
      return;
    }

    const { jobTitle, responsibleFor, fullName, phone,  typeRecord, parent } = this.form.value;
    const officeCells: IOfficeCells = {
      jobTitle,
      responsibleFor,
      fullName,
      phone,
    };

    const orderRecord = this.form.value.OrderRecord;
    const processedOrder = this.setOrderRecord(orderRecord);

    const post: IPost = {
      typeRecord,
      officeCells,
      date: new Date(),
      parent,
      order: processedOrder
    };
    if (this.form.value.typeRecord === 'header') {
      delete post.parent;
      delete post.order;
    }

    this.postService.savePost(post).subscribe(() => {
      this.fillParentsFromService();
      // this.router.navigate(['admin'])
      // this.alertService.success("Пост был создан");
    });
  }

  setOrderRecord(orderRecord: any): number | '' {
    // Преобразуем orderRecord в строку и проверяем, является ли она числом
    const orderValue = Number(orderRecord);

    // Если orderRecord пустой или не является числом, возвращаем 0
    if (isNaN(orderValue) || orderRecord === '') {
      return 1000;
    }

    // Возвращаем число, если оно корректное
    return orderValue;
  }

  setOrder(type: 'header' | 'normal') {
    const iPosts = this.postService.currentData$.getValue().filter(f => f.parent === this.form.value.parent);
    return this.getMaxOrder(iPosts) + 1;
  }

  getMaxOrder(postData: IPost[]) {
    if (postData.length) {
      return Math.max(...postData.map(post => +post.order ? +post.order : 0));
    }
    return 0;
  }

  fillParentsFromService() {
    // this.headers = this.exchangeService.fillParents();
    this.postService.getPosts(); // вначале закачиваем дату в сервис
    this.headers = this.exchangeService.fillParents(); // тут получаем дату из сервиса
  }

  filterHeaders() {
    this.postData.subscribe(data => {
      this.filteredHeaders = data
        .filter(post => post.typeRecord === 'header')
        .sort((a, b) => this.extractNumber(a.officeCells.jobTitle) - this.extractNumber(b.officeCells.jobTitle));
      if (this.filteredHeaders.length > 0) {
        this.form.patchValue({ parent: this.filteredHeaders[0].id });
      }
    });
  }

  extractNumber(jobTitle: string): number {
    const match = jobTitle.match(/^(\d+)/);
    return match ? parseInt(match[1], 10) : 0;
  }

  refreshPage() {
    this.location.go(this.location.path());
    window.location.reload();
  }
  logHelloWorld() {
  }
}
