import {Component, inject} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {PostService} from "../../../shared/services/post.service";

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent {
  authService = inject(AuthService);
  router = inject(Router);
  postService = inject(PostService);

  ngOnInit(): void {
    this.postService.setDashboardActive(true); // Устанавливаем состояние активности
  }

  ngOnDestroy(): void {
    this.postService.setDashboardActive(false); // Сбрасываем состояние активности
  }
  logout(event: Event) {
    event.preventDefault();
    this.postService.saveBoolean("isLogin", false)
    // this.authService.logout();
    this.router.navigate(['/']).then();
  }
}
