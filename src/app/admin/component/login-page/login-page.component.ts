import {Component, inject, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {PostService} from "../../../shared/services/post.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  form: FormGroup;
  postService = inject(PostService);
  router2 = inject(Router);
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      email: '',
      password: ''
    })
    this.asLogin()
  }

  asLogin () {
    let isLogin = this.postService.checkLogin()

    if (isLogin) {
      // Перенаправить на другой маршрут
      this.router.navigate(['/admin/dashboard']).then(r => {   });
    }
  }

  submit(): void {

    const login = this.form.value.email;
    const pass = this.form.value.password;

    if (login === "Admin" && pass === "1234567") {
      this.postService.saveBoolean("isLogin", true)
      this.router.navigate(['/admin/dashboard']).then(r => {   });
    }
    else {
    }
  }
}
