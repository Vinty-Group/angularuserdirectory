import { Component, inject, OnInit } from '@angular/core';
import { PostService } from '../../../shared/services/post.service';
import { IPost } from '../../interfaces';
import { ModalService } from '../../../shared/services/modal.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, Observable, switchMap } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {

  constructor() {}

  postService = inject(PostService);
  modalService = inject(ModalService);
  router = inject(Router);
  formModal: FormGroup;
  fb = inject(FormBuilder);

  start$: BehaviorSubject<string> = new BehaviorSubject<string>('init');
  postData: Observable<IPost[]>;

  typeRecord = [
    { id: 'normal', name: 'Нормальная' },
    { id: 'header', name: 'Заголовок' },
  ];

  postBeforeOpenModal: IPost;
  currentTab: number;

  ngOnInit(): void {
    this.asLogin();
  }

  asLogin() {
    let isLogin = this.postService.checkLogin();

    if (!isLogin) {
      return this.router.navigate(['/admin/login']).then(() => {});
    }
    this.initModalForm();
    return this.postData = this.start$.pipe(switchMap(() => this.postService.getOfficeAll()));
  }

  initModalForm(post?: IPost) {
    this.postBeforeOpenModal = post;
    this.formModal = this.fb.group({
      id: [post?.id],
      typeRecord: [post?.typeRecord || ''],
      jobTitle: [post?.officeCells?.jobTitle || '', Validators.required],
      responsibleFor: [post?.officeCells?.responsibleFor || ''],
      fullName: [post?.officeCells?.fullName || ''],
      phone: [post?.officeCells?.phone || ''],
      order: [post?.order || ''],
      parent: [post?.parent || '']
    });

    this.modalService.openModal();
  }

  updateModalForm(isFired: boolean = false) {
    const val = this.formModal.value;
    const officeCell = {
      jobTitle: val?.jobTitle,
      responsibleFor: val?.responsibleFor,
      fullName: val?.fullName,
      phone: val?.phone
    };
    const post: IPost = {
      id: val?.id,
      typeRecord: val?.typeRecord,
      officeCells: officeCell,
      date: new Date(),
      parent: val?.parent,
    };

    const orderRecord = this.formModal.value.order;
    post.order = this.setOrderRecord(orderRecord);
    post.firedStatus = isFired ? 2 : 1;

    return this.updateModalPost(post);
  }

  copyRecord() {
    const value = this.formModal.value;
    const officeCell = {
      jobTitle: value?.jobTitle,
      responsibleFor: value?.responsibleFor,
      fullName: value?.fullName,
      phone: value?.phone
    };
    const post: IPost = {
      id: value?.id,
      typeRecord: value?.typeRecord,
      officeCells: officeCell,
      date: new Date(),
      parent: value?.parent,
    };
    const orderRecord = this.formModal.value.order;
    post.order = this.setOrderRecord(orderRecord + 1);
    post.firedStatus = 1;

    return this.saveModalPost(post);
  }

  setOrderRecord(orderRecord: any): number | '' {
    const orderValue = Number(orderRecord);

    if (isNaN(orderValue) || orderRecord === '') {
      return 1000;
    }
    return orderValue;
  }

  deleteRecordModal() {
    const val = this.formModal.value;
    this.removeModal(val);
  }

  removeModal(post: IPost) {
    this.postService.remove(post.id)
      .subscribe(() => {
          this.modalService.closeModal();
          this.start$.next('saved');
        }
      );
  }

  removePost(post: IPost) {
    this.postService.remove(post.id).subscribe(() => {
      this.start$.next('deleted');
    });
  }

  updateModalPost(post: IPost) {
    this.postService.updatePost(post)
      .subscribe(() => {
          this.modalService.closeModal();
          this.start$.next('saved');
        }
      );
  }

  saveModalPost(post: IPost) {
    this.postService.savePost(post)
      .subscribe(() => {
          this.modalService.closeModal();
          this.start$.next('saved');
        }
      );
  }

  onTabChange(tabId: number) {
    this.currentTab = tabId;
    console.log(`Текущая вкладка: ${tabId}`);
  }
}
